# WeatherApp

***important**: OpenWeatherMap API is really unstable (by experience). Usually it throws status 502 and doesn't returns any data. The application handles it and gives
             feedback to the user. Though I write it here, too. To use mock server, comment out the 16th row of app.module.ts* 

###API calls

1. Get the weather state of cities by Id
http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric&appid=6c6d2aa112b8e2f256547ac57c4d1b68

    Retrieves data by the ids of the cities

    **Data:** 
    ```
    { 
      list:[
        {
          name:string,
          main: {temp:number}, // I assume that, this is the "average temperature", that was asked in the description
          wind: {speed:number} // I assume that, this is the "wind strength", that was asked in the description
        },
        ...
       ]
    }
    ```

    **Decisions:**  first I wanted to call the countries by names from the API(it would be nicer not to call city weather information by "hard-coded" values.. though I have created a config file
                    where it can be managed by any non-programmer. Openweathermap API seemed to be unstable when there were multiple calls on the same API endpoint. (there are no options
                    to call group of city names)
  
2. Get the hourly forecast of a city
api.openweathermap.org/data/2.5/forecast/hourly?q=London,us&appid=6c6d2aa112b8e2f256547ac57c4d1b68

      Retrieves hourly data by a name of a city
      
      **Data:**
      ```
      {
        dt: number;
        city: {name:string},
        list: [
          {
            main: {temp:number},
            weather: {main:string, icon:string} // e.g. main: "Cloudy", icon: "04d"
          },
          ...
        ]
      }
      ```

3. Icon resources: http://openweathermap.org/img/w/10d.png

    **Data:** string (the resource)


###Architectural Decisions

I have separated the application into 4 components
  1. AppComponent - wrapper component, it has a toolbar, but no other responsibility. It has a router-outlet, too (in AppRoutingModule the possible routes are defined)
  2. WeatherTableComponent - it is responsible for the management and representation of the cities' weather information (It contains a MatTable, powered by Angular Material)
                             [if the user clicks on a row, it is expanding and contains a ForecastComponent]
  3. ForecastComponent - responsible for the management of the forecasts of the chosen day
  4. DayCardComponent - it is responsible for the representation of a day's weather (in a card, which for I was using MatCard)
  
**Module decisions**
- There are two feature modules in the application. For WeatherTable and for Forecast components (Forecast includes DayCardComponent. It could also have a separated module, but
- I was using smart-dumb approach, by giving no BI responsibility to DayCard component)
- I did not create any shared modules, since the feature modules do not require any common "module groups", which for it would worth it to create a shared module.
- I did not create any lazy modules, because the application doesn't need it (if there were at least two different screens, it would make sense to implement e.g. WeatherTableModule as lazy module. But in this case it would be confusing and unnecessary)

**Service decisions**
- I created only one service, for the two requests and provided it in the AppModule (because WeatherTableComponent and ForecastComponent both use it). 
- I could have created
more services and separate them to WeatherTableModule and ForecastModule, so AppModule wouldn't have this dependency. But it is not reasonable at a small application like this.


###Tests

For the tests I was using Cypress.IO for functional tests and Karma + Jasmine for unit tests.

###Running

#####run project by
 - **ng serve** (you have to "npm install" when you first use the project, to download the node dependencies)
#####run tests by
 - Functional tests:
      - **npx cypress open** - (you have to "**npm install npx**", if you don't have npx (it is included to npm > v5.2))
      - the project also have to be runnig on a different console (**ng serve**") to run the functional cypress tests successfully
      - I have been using Cypress.IO for the functional tests. I was focusing here on the scenario, when one component indicates a change (calls a request) and the 
      outcome will appear on the body (by the snackbar, appearing)
 - Unit tests:
      - **ng test** - built in Angular command
      - every component has a spec file, which contains the unit test logic. The result coverage is visible in *weather-app/coverage/index.html* (100%)
                 

