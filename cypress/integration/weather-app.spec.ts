describe('Integrational tests', () => {

  describe('If the server is not available, it should show a snackbar', () => {
    beforeEach(() => {
      // overwrite "getCityInformation" server response by get-cities.json's content, return status 502
      cy.server()
        .route({
          status: 502,
          url: `/api/proxy/group*`,
          method: 'GET',
          response: 'fixture:get-cities.json'
        })
        // alias the request so we can use its reference later
        .as('groupError');
    });
    it('navigates to the page, checks if the request arrive', () => {
      // navigate to baseUrl(localhost:4200)
      cy.visit('')
        // wait for the request which we overwrote above
        .wait('@groupError');
    });
    it('should be loading', () => {
      // checks if weatherTableComponent's spinner is visible
      cy.get('.weather-table-spinner')
        .should('be.visible');
    });
    it('should show a snackbar', () => {
      // checks if snackbar is visible
      cy.get('.mat-snack-bar-container')
        .should('be.visible');
    });
    it('checks if snackbar contains the right text', () => {
      // checks if snackbar has the right content
      cy.get('.mat-snack-bar-container')
        .should('contain', 'It looks like some OpenWeatherMap services are unavailable. We are keep refreshing. ' +
          'Please wait or provide the MockAppService for mock datas!');
    });
  });
});
