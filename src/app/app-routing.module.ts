import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WeatherTableComponent } from './weather-table/weather-table.component';

/**
 * There is only one route in the application. If the user would like to navigate somewhere else, we redirect
 */
const routes: Routes = [
  {
    path: '',
    component: WeatherTableComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
