import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { MatSnackBar, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppService } from './app.service';
import { WeatherTableModule } from './weather-table/weather-table.module';
import { TokenInterceptor } from './utils/token.interceptor';
import { MockAppService } from './mock-app.service';

/**
 * Factory to provide AppService as MockAppService if necessary by one uncommenting
 * @param http
 * @param matSnackbar
 */
const appServiceFactory = (http: HttpClient, matSnackbar: MatSnackBar) => {
  /** Comment out the next row to use mock datas! **/
  // return new MockAppService();
  return new AppService(http, matSnackbar);
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,

    MatToolbarModule,
    WeatherTableModule
  ],
  providers: [
    { provide: AppService, useFactory: appServiceFactory, deps: [HttpClient, MatSnackBar] },
    // Http Token interceptor is used to inject information into Http requests
    { provide: HTTP_INTERCEPTORS, useExisting: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
