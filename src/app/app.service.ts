import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { CityInfoModel } from './model/city-info.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ForecastModel } from './model/forecast.model';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { retryOnError } from './utils/retry-on-error.operator';
import { ApiCityListInterface } from './model/api-city-list.model';
import { ApiCityObjectInterface } from './model/api-city-object.model';
import { ApiForecastListInterface } from './model/api-forecast-list.model';
import { ApiForecastObjectInterface } from './model/api-forecast-object.model';

@Injectable()
export class AppService {
  private snackbarRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(private http: HttpClient,
              private matSnackbar: MatSnackBar) { }

  getCityInformation(cityIds: number[]): Observable<CityInfoModel[]> {
    return this.http.get<ApiCityListInterface>(`${environment.baseUrl}/group`, {
      params: {
        id: cityIds.join(),
        units: 'metric'
      }
    }).pipe(
      // map the original structure to a readable and maintainable one, which we need
      map((datas: ApiCityListInterface) => {
        return datas.list.map((cityInfos: ApiCityObjectInterface) =>
          new CityInfoModel(cityInfos.id, cityInfos.name, cityInfos.main.temp, cityInfos.wind.speed));
      }),
      // if the API returns an error, retry and show a snackbar with information about the case (it happens quite frequently)
      retryOnError(this.snackbarRef, this.matSnackbar),
    );
  }

  getHourlyForecast(cityId: number): Observable<ForecastModel[]> {
    return this.http.get<ApiForecastListInterface>(`${environment.baseUrl}/forecast`, {
      params: {
        id: cityId.toString(),
        units: 'metric'
      }
    }).pipe(
      // map the original structure to a readable and maintainable one, which we need
      map((datas: ApiForecastListInterface) => {
        const forecasts: ForecastModel[] = [];
        datas.list.forEach((forecast: ApiForecastObjectInterface) => {
          forecasts.push(
            new ForecastModel(
              new Date(forecast.dt * 1000),
              datas.city.name,
              forecast.main.temp,
              forecast.weather[0].icon,
              forecast.weather[0].main));
        });
        return forecasts.slice(0, 5);
      }),
      // if the API returns an error, retry and show a snackbar with information about the case (it happens quite frequently)
      retryOnError(this.snackbarRef, this.matSnackbar)
    );
  }

}
