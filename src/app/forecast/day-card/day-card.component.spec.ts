import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { MatCardModule, MatProgressSpinnerModule, MatSnackBarModule } from '@angular/material';
import { DayCardComponent } from './day-card.component';
import { DatePipe, DecimalPipe } from '@angular/common';

describe('DayCardComponent', () => {
  let component: DayCardComponent;
  let fixture: ComponentFixture<DayCardComponent>;
  let de: DebugElement;
  // TestBed configuration
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatProgressSpinnerModule,
        MatSnackBarModule
      ],
      declarations: [
        DayCardComponent
      ]
    }).compileComponents();
  }));
  // declarations for easy access
  beforeEach(() => {
    fixture = TestBed.createComponent(DayCardComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
  });

  describe('Default state tests', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not be visible', () => {
      expect(de.query(By.css('mat-card'))).toBeFalsy();
    });
  });

  describe('give value to "cityId', () => {
    beforeEach(async () => {
      // it is just mock data
      component.forecast = { temperature: 11, icon: '10d', description: 'Cloudy', timeStamp: new Date(), cityName: 'Budapest' };
      fixture.detectChanges();
    });

    it('should be visible', () => {
      expect(de.query(By.css('mat-card'))).toBeTruthy();
    });

    it('should be loading', () => {
      expect(de.query(By.css('mat-spinner'))).toBeTruthy();
    });

    it('checks values on component', () => {
      // asserts if the same contents appear in the DOM as the ones which are stored in the component
      expect(de.query(By.css('.timestamp')).nativeElement.textContent)
        .toEqual(new DatePipe('en').transform(component.forecast.timeStamp, 'HH:mm'));
      expect(de.query(By.css('.temperature')).nativeElement.textContent)
        .toEqual(new DecimalPipe('en').transform(component.forecast.temperature, '1.0-1') + ' °C');
      expect(de.query(By.css('.description')).nativeElement.textContent).toEqual(component.forecast.description);
      expect(de.query(By.css('img')).nativeElement.src).toEqual(`http://openweathermap.org/img/w/${component.forecast.icon}.png`);
    });

    it('checks if on image load the spinner disappears', async () => {
      // imitate that the image resource loaded
      component.onImageLoaded();
      fixture.detectChanges();
      expect(de.query(By.css('mat-spinner'))).toBeFalsy();
    });
  });

});

