import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { ForecastModel } from '../../model/forecast.model';

@Component({
  selector: 'app-day-card',
  templateUrl: 'day-card.component.html',
  styleUrls: ['./day-card.component.scss'],
  // OnPush strategy, to avoid performance issues in the future
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayCardComponent {
  @Input() forecast: ForecastModel;
  imageLoaded = false;

  constructor(private cdr: ChangeDetectorRef) {}

  onImageLoaded() {
    this.imageLoaded = true;
    // necessary because of OnPush strategy (otherwise Angular wouldn't detect changes and the spinner would keep loading
    this.cdr.markForCheck();
  }
}
