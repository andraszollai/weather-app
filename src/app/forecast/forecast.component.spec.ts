import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { MatCardModule, MatProgressSpinnerModule, MatSnackBarModule } from '@angular/material';
import { AppService } from '../app.service';
import { MockAppService } from '../mock-app.service';
import { ForecastComponent } from './forecast.component';
import { DayCardComponent } from './day-card/day-card.component';
import { ForecastModel } from '../model/forecast.model';
import { forecastsMock } from '../utils/mocks/forecasts.mock';

describe('ForecastComponent', () => {
  let component: ForecastComponent;
  let fixture: ComponentFixture<ForecastComponent>;
  let de: DebugElement;
  // TestBed configuration
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatProgressSpinnerModule,
        MatSnackBarModule
      ],
      declarations: [
        ForecastComponent,
        DayCardComponent
      ],
      providers: [
        // mock the real service
        { provide: AppService, useClass: MockAppService }
      ]
    }).compileComponents();
  }));
  // declarations for easy access
  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  describe('Default state tests', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should be loading', () => {
      expect(de.query(By.css('.forecast-spinner'))).toBeTruthy();
    });

    it('should have no DayCardComponent', () => {
      expect(de.queryAll(By.css('app-day-card')).length).toEqual(0);
    });

  });

  describe('give value to "cityId', () => {
    beforeEach(() => {
      // it is just mock data
      component.cityId = -1;
      fixture.detectChanges();
    });

    // Waits until the "getHourlyForecast" service request returns
    beforeEach(async(async () => {
      await fixture.whenStable();
      fixture.detectChanges();
    }));

    it('should not be loading', () => {
      expect(de.query(By.css('.forecast-spinner'))).toBeFalsy();
    });

    it('should have 5 DayCardComponent', () => {
      expect(de.queryAll(By.css('app-day-card')).length).toEqual(5);
    });

    it('checks if "forecasts" attribute has the expected data', () => {
      // reproducing the expected data, based on forecastsMock JSON object
      let forecasts: ForecastModel[] = [];
      forecastsMock.list.forEach((forecast: {
        main: { temp: number };
        weather: { main: string, icon: string }[];
        dt: number;
      }) => {
        forecasts.push(
          new ForecastModel(
            new Date(forecast.dt * 1000),
            forecastsMock.city.name,
            forecast.main.temp,
            forecast.weather[0].icon,
            forecast.weather[0].main
          ));
      });
      // we only need the first 5 pieces (the mockObject has ~90)
      forecasts = forecasts.slice(0, 5);

      expect(component.forecasts).toEqual(forecasts);
    });
  });

});

