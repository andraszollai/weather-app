import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ForecastModel } from '../model/forecast.model';

@Component({
  selector: 'app-forecast',
  templateUrl: 'forecast.component.html',
  styleUrls: ['./forecast.component.scss'],
  // OnPush strategy, to avoid performance issues in the future
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForecastComponent implements OnInit {
  @Input() cityId: number;
  loading = true;
  forecasts: ForecastModel[] = [];

  constructor(private appService: AppService,
              private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.getHourlyForecast();
  }

  private getHourlyForecast() {
    this.appService.getHourlyForecast(this.cityId)
        .subscribe((forecasts: ForecastModel[]) => (this.forecasts = forecasts, this.cdr.markForCheck()));
  }
}
