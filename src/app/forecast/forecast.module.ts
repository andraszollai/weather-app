import { NgModule } from '@angular/core';

import { ForecastComponent } from './forecast.component';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatProgressSpinnerModule, MatSnackBarModule } from '@angular/material';
import { DayCardComponent } from './day-card/day-card.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
  ],
  exports: [
    // it is enough to export only ForecastComponent, since DayCardComponent is only used inside this module
    ForecastComponent
  ],
  declarations: [
    ForecastComponent,
    DayCardComponent
  ]
})
export class ForecastModule {}
