import { Injectable } from '@angular/core';
import { CityInfoModel } from './model/city-info.model';
import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { ForecastModel } from './model/forecast.model';
import { cityInfosMock } from './utils/mocks/city-infos.mock';
import { forecastsMock } from './utils/mocks/forecasts.mock';
import { ApiCityListInterface } from './model/api-city-list.model';
import { ApiCityObjectInterface } from './model/api-city-object.model';
import { ApiForecastListInterface } from './model/api-forecast-list.model';
import { ApiForecastObjectInterface } from './model/api-forecast-object.model';

/**
 * This service was created to fake server responses
 */
@Injectable()
export class MockAppService {
  getCityInformation(cityIds: number[]): Observable<CityInfoModel[]> {
    return of(cityInfosMock).pipe(
      // make the API call more realistic by delaying it
      delay(1000),
      // map the original structure to a readable and maintainable one, which we need
      map((datas: ApiCityListInterface) => {
        return datas.list.map((cityInfos: ApiCityObjectInterface) =>
          new CityInfoModel(cityInfos.id, cityInfos.name, cityInfos.main.temp, cityInfos.wind.speed));
      })
    );
  }

  getHourlyForecast(cityId: number): Observable<ForecastModel[]> {
    return of(forecastsMock).pipe(
      // make the API call more realistic by delaying it
      delay(1000),
      // map the original structure to a readable and maintainable one, which we need
      map((datas: ApiForecastListInterface) => {
        const forecasts: ForecastModel[] = [];
        datas.list.forEach((forecast: ApiForecastObjectInterface) => {
          forecasts.push(
            new ForecastModel(
              new Date(forecast.dt * 1000),
              datas.city.name,
              forecast.main.temp,
              forecast.weather[0].icon,
              forecast.weather[0].main));
        });
        // take only the first 5 hourly forecast (normally the API sends ~90)
        return forecasts.slice(0, 5);
      })
    );
  }

}
