import { ApiCityObjectInterface } from './api-city-object.model';

export interface ApiCityListInterface {
  list: ApiCityObjectInterface[];
}
