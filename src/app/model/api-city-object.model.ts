export interface ApiCityObjectInterface {
  id: number;
  name: string;
  main: { temp: number };
  wind: { speed: number };
}
