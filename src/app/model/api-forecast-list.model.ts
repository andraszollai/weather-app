import { ApiForecastObjectInterface } from './api-forecast-object.model';

export interface ApiForecastListInterface {
  city: { name: string };
  list: ApiForecastObjectInterface[];
}
