export interface ApiForecastObjectInterface {
  dt: number;
  main: { temp: number };
  weather: { main: string, icon: string }[];
}
