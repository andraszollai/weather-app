export class CityInfoModel {
  id: number;
  name: string;
  temperature: number;
  windStrength: number;

  constructor(id: number, name: string, temperature: number, windStrength: number) {
    this.id = id;
    this.name = name;
    this.temperature = temperature;
    this.windStrength = windStrength;
  }
}
