export class ForecastModel {
  timeStamp: Date;
  cityName: string;
  temperature: number;
  icon: string;
  description: string;

  constructor(timeStamp: Date, cityName: string, temperature: number, icon: string, description: string) {
    this.timeStamp = timeStamp;
    this.cityName = cityName;
    this.temperature = temperature;
    this.icon = icon;
    this.description = description;
  }
}
