/**
 * Configure the 5 chosen cities here, in CITIES constant
 * The application will use the id-s, which you provide here
 */
export const CITIES = [
  {
    id: 3054643,
    name: 'Budapest'
  },
  {
    id: 2759794,
    name: 'Amsterdam'
  },
  {
    id: 2643743,
    name: 'London'
  },
  {
    id: 3186886,
    name: 'Zagreb'
  },
  {
    id: 3067696,
    name: 'Prague'
  }
];

