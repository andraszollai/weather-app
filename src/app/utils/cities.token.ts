import { InjectionToken } from '@angular/core';

// injecting CITIES_TOKEN so it is part of the Angular application and is confidently providable by dependency injection
export const CITIES_TOKEN = new InjectionToken<{ id: number, name: string }[]>('CITIES_TOKEN');
