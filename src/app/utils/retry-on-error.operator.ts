import { Observable } from 'rxjs';
import { delay, retryWhen, tap } from 'rxjs/operators';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';

/**
 * Custom RxJS pipe, so there are no repeating RxJS code parts
 * @param snackbarRef
 * @param matSnackbar
 */
export const retryOnError = (snackbarRef: MatSnackBarRef<SimpleSnackBar>, matSnackbar: MatSnackBar) => <T>(source: Observable<T>) =>
  new Observable<T>(observer => {
    return source.pipe(
      retryWhen(errors =>
        errors.pipe(
          tap(() => {
            // only show snackbar, if it doesn't exist yet
            if (!snackbarRef) {
              snackbarRef = matSnackbar.open('It looks like some OpenWeatherMap services are unavailable. We are keep refreshing. Please wait or provide the MockAppService for mock datas!');
            }
          }),
          // wait 3 seconds until next retry
          delay(3000)
        )
      ),
      // if the request was successfull, dissmiss the snackbar
      tap(() => this.snackbarRef ? this.snackbarRef.dismiss() : undefined)
    ).subscribe({
      next(x) {
        observer.next(x);
      },
      error() {
        observer.error();
      },
      complete() {
        observer.complete();
      }
    });
  });
