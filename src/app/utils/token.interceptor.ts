import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

/**
 * Http Token interceptor is used to inject information into Http requests
 * It is provided into the root of the application, so is accessable to every component
 */
@Injectable({ providedIn: 'root' })
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}

  /**
   * Intercept every http call and provide an "appId" queryParam with the value provided by the environment
   * @param req
   * @param next
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const tokenReq = req.clone({
        params: req.params.set('appId', `${environment.appId}`)
      });
      return next.handle(tokenReq);
  }
}
