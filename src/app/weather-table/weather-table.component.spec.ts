import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { MatProgressSpinnerModule, MatSnackBarModule, MatTableModule } from '@angular/material';
import { ForecastModule } from '../forecast/forecast.module';
import { WeatherTableComponent } from './weather-table.component';
import { AppService } from '../app.service';
import { MockAppService } from '../mock-app.service';
import { CITIES_TOKEN } from '../utils/cities.token';
import { CITIES } from '../utils/cities.config';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { cityInfosMock } from '../utils/mocks/city-infos.mock';
import { CityInfoModel } from '../model/city-info.model';
import { DecimalPipe } from '@angular/common';
import { ApiCityObjectInterface } from '../model/api-city-object.model';

describe('WeatherTableComponent |', () => {
  let component: WeatherTableComponent;
  let fixture: ComponentFixture<WeatherTableComponent>;
  let de: DebugElement;
  // TestBed configuration
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTableModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        NoopAnimationsModule,

        ForecastModule
      ],
      declarations: [WeatherTableComponent],
      providers: [
        { provide: AppService, useClass: MockAppService },
        { provide: CITIES_TOKEN, useValue: CITIES }
      ]
    }).compileComponents();
  }));
  // declarations for easy access
  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherTableComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  describe('Default state tests', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should be loading', () => {
      expect(de.query(By.css('mat-spinner'))).toBeTruthy();
    });

  });

  describe('Waits until the "getCityInformation" service request returns |', () => {
    // waiting until the component is stable (there are no asynchronous calls in progress)
    beforeEach(async(async () => {
      await fixture.whenStable();
      fixture.detectChanges();
    }));

    it('should not be loading', () => {
      expect(de.query(By.css('mat-spinner'))).toBeFalsy();
    });

    it('should have table', () => {
      expect(de.query(By.css('table'))).toBeFalsy();
    });

    it('checks the header of the table', async () => {
      const headerCells = de.nativeElement.querySelectorAll('.mat-header-cell');
      // I have written it into the component (in a real situation, these would be internationalized keys
      ['Name', 'Temperature', 'Strength of wind'].forEach((columnName: string, index: number) => {
        expect(columnName).toEqual(headerCells[index].textContent);
      });
    });

    it('checks if table contains the right datas', () => {
      // get the datas which come from the MockService, too and map them
      const mapedCityInfos: CityInfoModel[] = cityInfosMock.list.map((cityInfos: ApiCityObjectInterface) =>
        new CityInfoModel(cityInfos.id, cityInfos.name, cityInfos.main.temp, cityInfos.wind.speed)
      );
      // Angular's DecimalPipe, which in the application is used as pipe in html files
      const numberPipe = new DecimalPipe('en');

      // query for every data table row
      const elementRows = de.queryAll(By.css('.element-row'));
      elementRows.forEach((elementRow: DebugElement, index: number) => {

        // get the reference of the elements of the data table row
        const matColumnName = elementRow.query(By.css('.mat-column-name'));
        const matColumnTemperature = elementRow.query(By.css('.mat-column-temperature'));
        const matColumnWindStrength = elementRow.query(By.css('.mat-column-windStrength'));

        // compare elements text content with the maped mockDatas that we would recieve from mock server
        expect(matColumnName.nativeElement.textContent.trim()).toEqual(mapedCityInfos[index].name);
        expect(matColumnTemperature.nativeElement.textContent.trim())
          .toEqual(numberPipe.transform(mapedCityInfos[index].temperature, '1.0-1') + ' °C');
        expect(matColumnWindStrength.nativeElement.textContent.trim())
          .toEqual(numberPipe.transform(mapedCityInfos[index].windStrength, '1.0-1') + ' km/h');
      });

    });

    it('expects ForecastComponent not to exist', () => {
      expect(de.nativeElement.querySelector('app-forecast')).toBeFalsy();
    });

    it('clicks on the first row and expects ForecastComponent to exist, close it, too', async () => {
      // find and click first data row
      const firstRow = de.nativeElement.querySelector('.element-row');
      firstRow.click();
      // waiting until the component is stable (there are no asynchronous calls in progress)
      await fixture.whenStable();
      fixture.detectChanges();
      // ForecastComponent should exist (=the row is expanded)
      expect(de.nativeElement.querySelector('app-forecast')).toBeTruthy();

      firstRow.click();
      await fixture.whenStable();
      fixture.detectChanges();
      // ForecastComponent should not exist (=the row is not expanded)
      expect(de.nativeElement.querySelector('app-forecast')).toBeFalsy();
    });

  });
});

