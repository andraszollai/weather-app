import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CityInfoModel } from '../model/city-info.model';
import { CITIES_TOKEN } from '../utils/cities.token';

@Component({
  selector: 'app-weather-table',
  templateUrl: 'weather-table.component.html',
  styleUrls: ['./weather-table.component.scss'],
  // Animations to make collapse transitions eye-appealing
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  // OnPush strategy, to avoid performance issues in the future
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherTableComponent implements OnInit {
  // it contains the data, which will appear in the body of the table
  dataSource: CityInfoModel[] = [];
  // identifiers of table columns
  columnsToDisplay = ['name', 'temperature', 'windStrength'];
  // chosen element
  expandedElement: any;

  constructor(private appService: AppService,
              // ChangeDetectorRef is necessary to use for
              private cdr: ChangeDetectorRef,
              @Inject(CITIES_TOKEN) private _CITIES_TOKEN: { id: number, name: string }[]) { }

  ngOnInit() {
    this.getCityInformation();
  }

  /**
   * Triggers if the user clicks on a row
   * @param element
   */
  onExpanded(element: any): void {
    this.expandedElement = this.expandedElement === element ? null : element;
  }

  /**
   * Retrieves data for the table content
   */
  private getCityInformation(): void {
    this.appService.getCityInformation(this._CITIES_TOKEN.map((city: { id: number, name: string }) => city.id))
        .subscribe((cityInfoModels: CityInfoModel[]) => (
            this.dataSource = cityInfoModels,
              this.cdr.markForCheck()
          ));
  }
}
