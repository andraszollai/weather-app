import { NgModule } from '@angular/core';

import { WeatherTableComponent } from './weather-table.component';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule, MatSnackBarModule, MatTableModule } from '@angular/material';
import { ForecastModule } from '../forecast/forecast.module';
import { CITIES } from '../utils/cities.config';
import { CITIES_TOKEN } from '../utils/cities.token';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,

    ForecastModule
  ],
  exports: [WeatherTableComponent],
  declarations: [WeatherTableComponent],
  // injecting CITIES_TOKEN so it is part of the Angular application and is confidently providable by dependency injection
  providers: [{ provide: CITIES_TOKEN, useValue: CITIES }],
})
export class WeatherTableModule {}
