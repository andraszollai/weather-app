/**
 * Cypress has problems with working with TypeScript, that's why we need this file and that's why I have to use ts-ignore
 */
//@ts-ignore
import Cypress from 'cypress';

declare global {
  namespace Cypress {
    interface Chainable {
      matchImageSnapshot: (options?: any) => void;
    }
  }
}
